# macrominds/provision/uberspace/web-prerequisites

Prepares an U7 Uberspace server with a specific php version: 

* e.g. php 7.4 or php 8.1

See https://gitlab.com/macrominds/provision/uberspace/web-domains for Uberspace web domain provisioning.

## Requirements

Setup an [U7 Uberspace](https://dashboard.uberspace.de/register) and provide an ssh access to it.

## Role Variables

See [defaults/main.yml](defaults/main.yml). 

* `web_prerequisites_tool_chdir:`: the folder from which to execute the following commands.
  Defaults to `/home/{{ ansible_facts.user_id }}`
* `web_prerequisites_tool_version_use_command:`: the command to change the version of a tool. 
  Defaults to `uberspace tools version use`
* `web_prerequisites_tool_version_php`: the php version to use.
  Defaults to `7.4`
* `web_prerequisites_tool_version_command_parameter:`: the parameter to use with the command 
  Defaults to `php {{ web_prerequisites_tool_version_php }}`
* `web_prerequisites_user_facts_versions_yml_path:`: path to userfacts file on remote site. 
  Defaults to `/opt/uberspace/userfacts/{{ ansible_facts.user_id }}/versions.yml`
* `web_prerequisites_tool_command_warnings`: if warnings should be issued for typical command misuse like touch, ls, etc.
  This is used to turn off warnings during testing, because the mocked commands would trigger a warning. 
  Defaults to `true`

## Example Playbook

### Prerequisites

In your project, provide the following files:

ansible.cfg

```ini
[defaults]
roles_path = $PWD/galaxy_roles:$PWD/roles
```

requirements.yml
 
```yaml
- src: git+https://gitlab.com/macrominds/provision/uberspace/web-prerequisites.git
  path: roles
  name: web-prerequisites
```

And run `ansible-galaxy install -r requirements.yml` to install 
or `ansible-galaxy install -r requirements.yml  --force` to upgrade.

### Playbook

```yaml
- hosts: all
  roles:
    - role: web-prerequisites
```

customize the php version:

```yaml
- hosts: all
  roles:
    - role: web-prerequisites
      web_prerequisites_tool_version_php: '8.1'
```

## Testing

Test this role with `molecule test --all`.

We're working with mocked uberspace commands and with fixtures of the uberspace userfacts versions.yml file.

This is why we copy a fixture in almost all `molecule/**/playbook.yml` files after having performed the 
actions. The versions.yml fixture files reflect the expected content of the userfacts versions.yml file.

## License

ISC

## Author Information

This role was created in 2019 by Thomas Praxl.
