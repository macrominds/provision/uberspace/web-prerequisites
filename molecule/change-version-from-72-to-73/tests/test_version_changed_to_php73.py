from testinfra.host import Host


def test_nothing_has_been_touched(host):
    assert get_expected_entries() == ls_home_as_array(host)


def get_expected_entries():
    return ["php 7.2", "php 7.3"]


def ls_home_as_array(host: Host):
    return host.ansible(
        "command",
        "ls /home/ansible",
        check=False
    )["stdout_lines"]
